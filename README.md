单例模式
==============

#Objective-C 单例模式
实现方式一般分为两种：Non-ARC(非ARC)和ARC+GCD

[相关资料](http://beyondvincent.com/blog/2013/05/09/20/)

###Non-ARC(非ARC)
官方简洁的代码实现

```Objective-c

static AccountManager *DefaultManager = nil;
 
+ (AccountManager *)defaultManager {
    if (!DefaultManager) DefaultManager = [[self allocWithZone:NULL] init];
    return DefaultManager;
}

```

为了保证在实例化的时候是线程安全的，使用@synchronized来创建互斥锁来实现

```Objective-C

static BVNonARCSingleton *sharedInstance = nil;

// 获取一个sharedInstance实例，如果有必要的话，实例化一个
+ (BVNonARCSingleton *)sharedInstance {
    @synchronized (self)
    {
      if(sharedInstance == nil)
      {
        sharedInstance = [[super allocWithZone:NULL] init];
      }
    }
    return sharedInstance;
}

// 当第一次使用这个单例时，会调用这个init方法。
- (id)init
{
    self = [super init];
    if (self) {
        // 通常在这里做一些相关的初始化任务
    }
    return self;
}

// 这个dealloc方法永远都不会被调用--因为在程序的生命周期内容，该单例一直都存在。（所以该方法可以不用实现）
-(void)dealloc
{
    [super dealloc];
}

// 通过返回当前的sharedInstance实例，就能防止实例化一个新的对象。
+ (id)allocWithZone:(NSZone*)zone {
    return [[self sharedInstance] retain];
}

// 同样，不希望生成单例的多个拷贝。
- (id)copyWithZone:(NSZone *)zone {
    return self;
}

// 什么也不做——该单例并不需要一个引用计数（retain counter）
- (id)retain {
    return self;
}

// 替换掉引用计数——这样就永远都不会release这个单例。
- (NSUInteger)retainCount {
    return NSUIntegerMax;
}

// 该方法是空的——不希望用户release掉这个对象。
- (oneway void)release {

}

//除了返回单例外，什么也不做。
- (id)autorelease {
    return self;
}

```

###ARC+GCD

调用Grand Central Dispatch (GCD)中的dispatch_once方法就可以确保只被实例化一次。并且该方法是线程安全的，我们不用担心在不同的线程中，会获得不同的实例

```Objective-C
+ (BVARCSingleton *) sharedInstance
{
    static  BVARCSingleton *sharedInstance = nil ;
    static  dispatch_once_t onceToken;  // 锁
    dispatch_once (& onceToken, ^ {     // 最多调用一次
        sharedInstance = [[self  alloc] init];
    });
    return  sharedInstance;
}

// 当第一次使用这个单例时，会调用这个init方法。
- (id)init
{
    self = [super init];

    if (self) {
        // 通常在这里做一些相关的初始化任务
    }

    return self;
}
```

为了简化使用ARC+GCD来创建单例，可以定义下面这样的一个宏：

```Objective-C
#define DEFINE_SHARED_INSTANCE_USING_BLOCK(block) \
static dispatch_once_t onceToken = 0; \
__strong static id sharedInstance = nil; \
dispatch_once(&onceToken, ^{ \
sharedInstance = block(); \
}); \
return sharedInstance; \
```

实例化的实现方法如下所示：
```Objective-C
+ (BVARCSingleton *) sharedInstance
{
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[self alloc] init];
    });
}

```
当然，在ARC中，不用GCD也是可以做到线程安全的，跟之前非ARC代码中使用@synchronized一样，如下代码：


```
#!objective-c
// 不使用GCD，通过@synchronized
static  BVARCSingleton *sharedInstance = nil ;

+ (BVARCSingleton *)sharedInstance {
    @synchronized (self)
    {
        if(sharedInstance == nil)
        {
           sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

```



#Swift 单例模式

[相关资料](https://github.com/hpique/SwiftSingleton)

### Approach A: Global constant

```swift
let _SingletonASharedInstance = SingletonA()

class SingletonA  {

    class var sharedInstance : SingletonA {
        return _SingletonASharedInstance
    }
    
}
```
We use a global constant because class constants are not yet supported.

This approach supports lazy initialization because Swift lazily initializes global constants (and variables), and is thread safe by the definition of `let`.

### Approach B: Nested struct

```swift
class SingletonB {
    
    class var sharedInstance : SingletonB {
        struct Static {
            static let instance : SingletonB = SingletonB()
        }
        return Static.instance
    }
    
}
```

Unlike classes, structs do support static constants. By using a nested struct we can leverage its static constant as a class constant.

### Approach C: dispatch_once

The traditional Objective-C approach ported to Swift.

```swift
class SingletonC {
    
    class var sharedInstance : SingletonC {
        struct Static {
            static var onceToken : dispatch_once_t = 0
            static var instance : SingletonC? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = SingletonC()
        }
        return Static.instance!
    }
}
```